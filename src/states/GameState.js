import Phaser from 'phaser';
import Town from '../prefabs/TownModel';
import Building from '../prefabs/Building';
import buttonData from '../../assets/data/buttons.json';

export default () => {
    const obj = new Phaser.State();
    obj.init = function () {
        // Game constants
        this.STEP = 2;
        this.game.physics.arcade.gravity.y = 0;
    };
    obj.clearSelection = function () {
        this.isDraggingMapBlocked = false;
        this.isDraggingMap = false;
        this.isBuildingBtnActive = false;
        this.isDraggingBuilding = false;
        this.selectedBuilding = null;
        if (this.shadowBuilding) {
            this.shadowBuilding.kill();
        }
        this.refreshStats();
        this.buttons.setAll('alpha', 1);
    };
    obj.clickBuildButton = function (btn) {
        const button = btn;
        this.clearSelection();
        if (this.town.stats.money >= button.buildingData.cost) {
            button.alpha = 0.5;
            this.selectedBuilding = button.buildingData;
            this.isBuildingBtnActive = true;
        }
    };
    obj.initGui = function () {
        const style = { font: '14px Arial', fill: '#fff' };
        this.moneyIcon = this.add.sprite(10, 10, 'money');
        this.moneyIcon.fixedToCamera = true;
        this.moneyLabel = this.add.text(45, 15, '0', style);
        this.moneyLabel.fixedToCamera = true;

        this.foodIcon = this.add.sprite(100, 10, 'food');
        this.foodIcon.fixedToCamera = true;
        this.foodLabel = this.add.text(135, 15, '0', style);
        this.foodLabel.fixedToCamera = true;

        this.populationIcon = this.add.sprite(190, 10, 'population');
        this.populationIcon.fixedToCamera = true;
        this.populationLabel = this.add.text(225, 15, '0', style);
        this.populationLabel.fixedToCamera = true;

        this.jobsIcon = this.add.sprite(280, 10, 'jobs');
        this.jobsIcon.fixedToCamera = true;
        this.jobsLabel = this.add.text(315, 15, '0', style);
        this.jobsLabel.fixedToCamera = true;

        this.buttonData = buttonData;
        this.buttons = this.add.group();
        this.buttonData.forEach((elementData, index) => {
            const button = new Phaser.Button(
                this.game,
                this.game.width - 60 - (60 * index),
                this.game.height - 60,
                elementData.btnAsset,
                this.clickBuildButton,
                this
            );
            button.fixedToCamera = true;
            button.buildingData = elementData;
            this.buttons.add(button);
        });
    };
    obj.refreshStats = function () {
        this.moneyLabel.text = Math.round(this.town.stats.money);
        this.populationLabel.text = `${Math.round(this.town.stats.population)}/${Math.round(this.town.stats.housing)}`;
        this.foodLabel.text = Math.round(this.town.stats.food);
        this.jobsLabel.text = Math.round(this.town.stats.jobs);
    };
    obj.create = function () {
        const house = Building(this, 100, 100, { asset: 'house', housing: 100 });
        const farm = Building(this, 200, 200, { asset: 'crops', food: 100 });
        const factory = Building(this, 200, 300, { asset: 'factory', jobs: 20 });

        this.background = this.add.tileSprite(0, 0, 1200, 800, 'grass');
        this.game.world.setBounds(0, 0, 1200, 800);
        this.buildings = this.add.group();
        this.buildings.add(house);
        this.buildings.add(farm);
        this.buildings.add(factory);
        this.town = Town(
            {},
            {
                population: 100,
                food: 200,
                money: 100
            },
            this.buildings
        );

        // Update simulation
        this.simulationTimer = this.game.time.events.loop(
            Phaser.Timer.SECOND * this.STEP,
            this.simulationStep,
            this
        );
        this.initGui();
        this.refreshStats();
    };
    obj.update = function () {
        if (!this.isDraggingMapBlocked) {
            // start dragging
            if (!this.isDraggingMap) {
                if (this.game.input.activePointer.isDown) {
                    this.isDraggingMap = true;
                    this.startDragPoint = {};
                    this.startDragPoint.x = this.game.input.activePointer.position.x;
                    this.startDragPoint.y = this.game.input.activePointer.position.y;
                }
            } else if (this.isDraggingMap) {
                this.endDragPoint = {};
                this.endDragPoint.x = this.game.input.activePointer.position.x;
                this.endDragPoint.y = this.game.input.activePointer.position.y;
                this.game.camera.x += this.startDragPoint.x - this.endDragPoint.x;
                this.game.camera.y += this.startDragPoint.y - this.endDragPoint.y;
                // after update, take a new starting point so the camera will update again
                this.startDragPoint.x = this.game.input.activePointer.position.x;
                this.startDragPoint.y = this.game.input.activePointer.position.y;
                // stop dragging map
                if (this.game.input.activePointer.isUp) {
                    this.isDraggingMap = false;
                }
            }
        }

        if (this.isBuildingBtnActive && this.game.input.activePointer.isDown) {
            this.isDraggingMapBlocked = true;
            this.isDraggingBuilding = true;
        }

        if (this.isDraggingBuilding) {
            const pointerWX = this.game.input.activePointer.worldX;
            const pointerWY = this.game.input.activePointer.worldY;

            if (!this.shadowBuilding || !this.shadowBuilding.alive) {
                const { asset } = this.selectedBuilding;
                this.shadowBuilding = this.add.sprite(pointerWX, pointerWY, asset);
                this.shadowBuilding.alpha = 0.7;
                this.shadowBuilding.anchor.setTo(0.5);
                this.game.physics.arcade.enable(this.shadowBuilding);
            }
            this.shadowBuilding.x = pointerWX;
            this.shadowBuilding.y = pointerWY;
        }

        if (this.isDraggingBuilding && this.game.input.activePointer.isUp) {
            if (this.canBuild()) {
                this.town.stats.money -= this.selectedBuilding.cost;
                this.createBuilding(
                    this.game.input.worldX,
                    this.game.input.worldY,
                    this.selectedBuilding
                );
            }
            this.clearSelection();
        }
    };
    obj.createBuilding = function (x, y, data) {
        this.buildings.add(Building(this, x, y, data));
    };
    obj.canBuild = function () {
        return !this.game.physics.arcade.overlap(this.shadowBuilding, this.buildings);
    };
    obj.simulationStep = function () {
        this.town.step();
        this.refreshStats();
    };
    return obj;
};
