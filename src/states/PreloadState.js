import Phaser from 'phaser';
import grass from '../../assets/images/grass.png';
import tree from '../../assets/images/tree.png';
import crops from '../../assets/images/crops.png';
import factory from '../../assets/images/factory.png';
import house from '../../assets/images/house.png';
import food from '../../assets/images/food.png';
import money from '../../assets/images/money.png';
import population from '../../assets/images/population.png';
import jobs from '../../assets/images/worker.png';
import buttonFarm from '../../assets/images/button_farm.png';
import buttonFactory from '../../assets/images/button_factory.png';
import buttonHouse from '../../assets/images/button_house.png';

const imagesAssets = [
    grass,
    tree,
    crops,
    factory,
    house,
    food,
    money,
    population,
    jobs,
    buttonFactory,
    buttonFarm,
    buttonHouse
];
const imagesNames = ['grass', 'tree', 'crops', 'factory', 'house', 'food', 'money', 'population', 'jobs', 'buttonFactory', 'buttonFarm', 'buttonHouse'];

export default () => {
    const obj = new Phaser.State();
    obj.preload = function () {
        this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bar');
        this.preloadBar.anchor.setTo(0.5);
        this.preloadBar.scale.setTo(100, 1);

        this.load.setPreloadSprite(this.preloadBar);

        this.load.images(imagesNames, imagesAssets);
    };
    obj.create = function () {
        this.state.start('Game');
    };
    return obj;
};
