/* eslint-disable no-unused-vars */
/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
import PIXI from 'pixi';
import P2 from 'p2';
import Phaser from 'phaser';
import Boot from './states/BootState';
import Preload from './states/PreloadState';
import Game from './states/GameState';

import getGameLandscapeDimensions from './utils/scaler';

const HipsterTown = {};

HipsterTown.dim = getGameLandscapeDimensions(800, 600);

HipsterTown.game = new Phaser.Game(HipsterTown.dim.width, HipsterTown.dim.height, Phaser.AUTO);
HipsterTown.game.state.add('Boot', Boot());
HipsterTown.game.state.add('Preload', Preload());
HipsterTown.game.state.add('Game', Game());
HipsterTown.game.state.start('Boot');
