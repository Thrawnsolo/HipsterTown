export default (coefs, initialStats, buildings) => {
    const obj = {};
    obj.stats = {};
    obj.coefs = {};
    obj.buildings = buildings;

    obj.stats.population = initialStats.population;
    obj.stats.food = initialStats.food;
    obj.stats.money = initialStats.money;
    obj.stats.housing = initialStats.housing || 0;

    obj.coefs.populationGrowth = coefs.populationGrowth || 1.02;
    obj.coefs.foodConsumption = coefs.foodConsumption || 1;
    obj.coefs.productivityPerPerson = coefs.productivityPerPerson || 0.5;

    obj.updateBuildingProduction = function () {
        let housing = 0;
        let jobs = 0;
        this.buildings.forEach((building) => {
            if (building.housing) {
                housing += building.housing;
            }
            if (building.food) {
                this.stats.food += building.food;
            }
            if (building.jobs) {
                jobs += building.jobs;
            }
        });
        this.stats.housing = housing;
        this.stats.jobs = jobs;
    };
    obj.step = function () {
        // Update building productions
        this.updateBuildingProduction();
        this.stats.population = this.stats.population * this.coefs.populationGrowth;
        // Population cannot be more than the number of housing spaces
        this.stats.population = Math.min(this.stats.population, this.stats.housing);
        // update food = new food = old food + food prod - food consumption
        this.stats.food -= this.stats.population * this.coefs.foodConsumption;

        // if food is negative, decrease population
        if (this.stats.food < 0) {
            this.stats.population += this.stats.food / this.coefs.foodConsumption;
            this.stats.food = 0;
        }
        // Industrial output
        this.stats.money +=
            Math.min(this.stats.population, this.stats.jobs) * this.coefs.productivityPerPerson;
    };

    obj.updateBuildingProduction();
    return obj;
};
