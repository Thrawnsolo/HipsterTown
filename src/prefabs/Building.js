import Phaser from 'phaser';

export default (state, x, y, data) => {
    const obj = new Phaser.Sprite(state.game, x, y, data.asset);
    obj.game = state.game;
    obj.state = state;

    obj.food = data.food || 0;
    obj.jobs = data.jobs || 0;
    obj.housing = data.housing || 0;

    obj.anchor.setTo(0.5);
    obj.game.physics.arcade.enable(obj);
    return obj;
};
